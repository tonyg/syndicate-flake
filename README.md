# Syndicate Nix flake

To add to your local flake registry:
```sh
nix registry add syndicate "git+https://git.syndicate-lang.org/ehmry/syndicate-flake?ref=trunk"
```

## NixOS service

### Importing
To import the NixOS module:
```nix
{
  # /etc/nixos/flake.nix
  inputs.syndicate.url = "git+https://git.syndicate-lang.org/ehmry/syndicate-flake?ref=trunk";

  outputs = { self, nixpkgs, syndicate }: {

    nixosConfigurations.myhost = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ./configuration.nix
        syndicate.nixosModules.syndicate-server
      ];
    };

  };

}
```

If `/etc/nixos` is not a flake then you may be able to use the `getFlake` builtin
to retrieve the module.
```nix
{ config, lib, pkgs, ... }:

{
  imports = [ (builtins.getFlake "syndicate").nixosModules.syndicate-server ];
}
```

## Configuration
```nix
{ config, lib, pkgs, ... }:

{
  services.syndicate-server = {
    enable = true;
    # A socket at /run/syndicate/ds is enable by default.
    tcpListeners = [{
      address = "127.0.0.1";
      port = 3232;
    }];
  };

}

```

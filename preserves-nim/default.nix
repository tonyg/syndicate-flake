{ lib, nimPackages, fetchFromGitea }:
with nimPackages;

let
  bigints = fetchNimble {
    pname = "bigints";
    version = "0.5.0";
    hash = "sha256-zMTXXC0lYVzXaKl8eC/SA/CymRgovgzxBdvEn1VE9p0=";
  };

  compiler = fetchNimble {
    pname = "compiler";
    version = "1.4.8";
    hash = "sha256-OaSe9Bn/+rjVk4pJKjjwI9rpnDm/T7lRmKeh/DOPn38=";
  };

  npeg = fetchNimble {
    pname = "npeg";
    version = "0.24.1";
    hash = "sha256-AKYDrR38345CQxV7kLvglGmkc/m8EX6TGtaFckJL3Dc=";
  };

in buildNimPackage rec {
  outputs = [ "out" "dev" ];

  pname = "preserves";
  version = "2.0.0";
  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = "preserves-nim";
    rev = "v${version}";
    sha256 = "sha256-DRncKqq9+2WR1V1EeTnAh7ObCUNqfz7hp8onrCi1mlA=";
  };
  buildInputs = [ compiler bigints npeg ];
  postInstall = ''
    pushd $out/bin
    for link in preserves_decode preserves_from_json preserves_to_json;
    do ln -s  preserves_encode $link
    done
    popd
  '';
  doCheck = true;
}

{ lib, rustPlatform, rust, fetchgit, pkg-config, openssl }:

rustPlatform.buildRustPackage rec {
  pname = "syndicate-rs";
  version = "2021-09-01";
  src = fetchgit {
    url = "https://git.syndicate-lang.org/syndicate-lang/syndicate-rs.git";
    rev = "622115e13c69397dfa0beb433fdaf382949252b3";
    sha256 = "sha256-7XcCiDlUntLRB0MLYk+57RSe4f6NyqRa/Jc42xrPOnQ=";
  };
  cargoHash = "sha256-sagyE3yCmSNwqgZrgnw1ar7S37iZhVYYpn3WtIoBIU8=";
  nativeBuildInputs = [ rust pkg-config ];
  buildInputs = [ openssl ];

  meta = {
    description =
      "Rust implementation of Dataspaces, Syndicate, and a high-speed networked dataspace broker.";
    homepage = "https://git.syndicate-lang.org/syndicate-lang/syndicate-rs/";
    maintainers = with lib.maintainers; [ ehmry ];
  };
}

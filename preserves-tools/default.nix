{ lib, rustPlatform, rust, fetchCrate }:

rustPlatform.buildRustPackage rec {
  pname = "preserves-tool";
  # use the name of the actual utility so that
  # nix run …#preserves-tools works as expected.

  version = "1.0.0";
  src = fetchCrate {
    pname = "preserves-tools"; # name of the crate
    inherit version;
    sha256 = "10vy7pmdbxkcwbr1qwqqv1lfi622pk8ncbp1hn5jrddxrv92q4ph";
  };
  cargoHash = "sha256-h6ulvjTDTj8/fmzEn1YgZKmze4jwpmGjcSq78hmq2cU=";
  nativeBuildInputs = [ rust ];

  meta = {
    description = "Swiss army knife for working with Preserves documents";
    homepage = "https://preserves.gitlab.io/preserves/doc/preserves-tool.html";
    maintainers = with lib.maintainers; [ ehmry ];
  };
}

{ config, lib, pkgs, ... }:
with lib;

{
  options.services.syndicate-server = {
    enable = mkEnableOption "the Syndicate dataspace server";

    package = mkOption {
      default = pkgs.syndicate-rs;
      defaultText = "pkgs.syndicate-rs";
      type = types.package;
      description = "The package to use for the Syndicate dataspace server.";
    };

    tcpListeners = mkOption {
      default = [ ];
      example = [{
        address = "0.0.0.0";
        port = 8001;
      }];
      type = with types;
        listOf (submodule {
          options = {
            address = mkOption { type = str; };
            port = mkOption { type = port; };
          };
        });
      description = "TCP ports to listen for connections on.";
    };

    unixListeners = mkOption {
      default = [ "/run/syndicate/ds" ];
      type = types.listOf types.path;
      description = "Sockets to listen for connections on.";
    };

  };

  config = let
    cfg = config.services.syndicate-server;
    configDir = "/run/syndicate/config";
    requireServiceRelayListener = spec:
      "<require-service <relay-listener ${spec}>>";
  in mkIf cfg.enable {

    systemd.services.syndicate-server = {
      description = "Syndicate dataspace server";
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStartPre = "${pkgs.coreutils}/bin/mkdir -p ${configDir}";
        ExecStart = "${cfg.package}/bin/syndicate-server --config ${configDir}";
        AmbientCapabilities = [ "CAP_NET_BIND_SERVICE" ];
        DynamicUser = true;
        Restart = "always";
        RuntimeDirectory = "syndicate";
      };
    };

    systemd.services.syndicate-server-unix-listeners = {
      description = "Configure Syndicate unix listeners";
      after = [ "syndicate-server.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = let
          prsFile = builtins.toFile "unix-listeners.pr"
            (lib.strings.concatMapStrings
              (path: requireServiceRelayListener ''<unix "${path}">'')
              cfg.unixListeners);
        in "${pkgs.coreutils}/bin/cp ${prsFile} ${configDir}/nixos-unix-listeners.pr";
        ExecStop =
          "${pkgs.coreutils}/bin/rm ${configDir}/nixos-unix-listeners.pr";
        RemainAfterExit = true;
      };
    };

    systemd.services.syndicate-server-tcp-listeners = {
      description = "Configure Syndicate TCP listeners";
      after = [ "syndicate-server.service" "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = let
          prsFile = builtins.toFile "tcp-listeners.pr"
            (lib.strings.concatMapStrings ({ address, port }:
              requireServiceRelayListener
              ''<tcp "${address}" ${toString port}>'') cfg.tcpListeners);
        in "${pkgs.coreutils}/bin/cp ${prsFile} ${configDir}/nixos-tcp-listeners.pr";
        ExecStop =
          "${pkgs.coreutils}/bin/rm ${configDir}/nixos-tcp-listeners.pr";
        RemainAfterExit = true;
      };
    };

  };
}

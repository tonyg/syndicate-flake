{
  description = "Syndicate utilities";

  inputs.rust.url = "github:oxalica/rust-overlay";

  outputs = { self, nixpkgs, rust }:
    let
      forEachSystem = nixpkgs.lib.genAttrs [ "aarch64-linux" "x86_64-linux" ];
      libOverlay = import ./lib.nix;
    in {
      lib = nixpkgs.lib.extend libOverlay;

      overlay = final: prev:
        let rust' = (prev.extend rust.overlay).rust-bin.nightly.latest.default;
        in {
          lib = prev.lib.extend libOverlay;
          preserves-nim = final.callPackage ./preserves-nim { };
          preserves-tools =
            final.callPackage ./preserves-tools { rust = rust'; };
          syndicate-rs = final.callPackage ./syndicate-rs { rust = rust'; };
        };

      packages = forEachSystem (system:
        let pkgs = nixpkgs.legacyPackages.${system}.extend self.overlay;
        in with pkgs; { inherit preserves-nim preserves-tools syndicate-rs; });

      nixosModules.syndicate-server =
        # A little hack to apply our overlay to this module only.
        let f = import ./nixos/syndicate-server.nix;
        in { config, lib, pkgs, ... }:
        f {
          inherit config lib;
          pkgs = pkgs.extend self.overlay;
        };
    };
}
